/**
 * 设置cookie
 * @param key
 * @param val
 * @param time
 */
export function setCookie(key: string, val: string, time?: number) {
  const date = new Date();
  const expiresDays = time || 1;
  date.setTime(date.getTime() + expiresDays * 24 * 3600 * 1000);
  document.cookie = `${key}=${val};expires=${(date as any).toGMTString()}`;
}
