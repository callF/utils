
interface IRequestParams {
  [key: string]: any;
}

// 根据qeury对象生成query string
export function genQuery(params?: IRequestParams) {
  if (!params) {
    return '';
  }
  let index = 0;
  let query = '';
  Object.keys(params).forEach((name) => {
    if (params[name] !== undefined && params[name] !== null) {
      query += index === 0 ? '?' : '&';
      const value = encodeURIComponent(params[name]);
      query += `${name}=${value}`;
      index += 1;
    }
  });
  return query;
}

// 获取query参数值
// 获取url中的query项
export function getQueryString(name: string) {
  const reg = new RegExp(`(^|&)${name}=([^&]*)(&|$)`, 'i');
  const r = window.location.search.substr(1).match(reg);
  if (r != null) {
    return unescape(r[2]);
  }
  return null;
}